#include <iostream>
#include <string>

void PrintIntro();
void GetGuess();
void PlayGame();
void PrintGuess();
bool AskToPlayAgain();

// Global variables
std::string CurrentGuess;

int main()
{
	bool bKeepGoing;
	do 
	{
		// Entry point for the application
		PrintIntro();
		// Starts the game
		PlayGame();
		bKeepGoing = AskToPlayAgain();
		std::cout << std::endl;
	} while (bKeepGoing);
	
	
	return 0;
}

void PlayGame()
{

	constexpr int NUMBER_OF_TURNS = 5;

	// Prompt for guess
	for (int i = 0; i < NUMBER_OF_TURNS; i++)
	{
		GetGuess();
		PrintGuess();
	}

	return;
}

void PrintIntro()
{
	constexpr int WORD_LENGTH = 9;

	std::cout << "Welcome to Bulls and Cows!\n";
	std::cout << "Can you guess the " << WORD_LENGTH << " letter isogram I am thinking of...\n";

	return;
}

void GetGuess()
{
	// get a guess from the player
	std::string Guess = "";

	std::cout << "Take a guess: "; std::getline(std::cin, Guess);

	CurrentGuess = Guess;

	return;
}

void PrintGuess()
{
	// repeat the guess back to them
	std::cout << std::endl << "Your guess was: " << CurrentGuess << std::endl;
	std::cout << std::endl;

	return;
}

bool AskToPlayAgain()
{
	std::cout << "Do you want to play again? Type yes or no: ";
	std::string Response = " ";
	std::getline(std::cin, Response);

	if (Response[0] == 'y' || Response[0] == 'Y')
	{
		return true;
	}
	else
	{
		return false;
	}
}
